package com.expedia.test.receipt_service.dto;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnit4.class)
public class TaxedItemTest {

    @Test
    public void shouldCalculateTotalPriceAsCumulativeForEachProduct() {
        TaxedItem sut = new TaxedItem(new Item(2, "product", 1.0), 0.0);

        Double totalPrice = sut.getTotalPrice();

        assertThat(totalPrice).isEqualTo(2.0);
    }

    @Test
    public void shouldAddTaxToTheCumulativeTotalPrice() {
        TaxedItem sut = new TaxedItem(new Item(3, "product", 1.0), 5.0);

        Double totalPrice = sut.getTotalPrice();

        assertThat(totalPrice).isEqualTo(8.0);
    }

    @Test
    public void shouldRoundTotalPriceToUpper05() {
        TaxedItem sut = new TaxedItem(new Item(1, "product", 1.111), 1.222);

        Double totalPrice = sut.getTotalPrice();

        assertThat(totalPrice).isEqualTo(2.35);
    }
}
