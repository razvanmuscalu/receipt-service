package com.expedia.test.receipt_service;

import com.expedia.test.receipt_service.dto.Item;
import com.expedia.test.receipt_service.dto.TaxedItem;
import junitparams.JUnitParamsRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(JUnitParamsRunner.class)
public class ReceiptServiceTest {

    @Mock
    private InputConverter inputConverter;

    @Mock
    private TaxCalculator taxCalculator;

    @Mock
    private OutputConverter outputConverter;

    @InjectMocks
    private ReceiptService sut;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void shouldApplyTaxToEveryItem() {
        when(inputConverter.convert(anyString())).thenReturn(asList(mock(Item.class), mock(Item.class)));

        sut.calculate(anyString());

        verify(taxCalculator, times(2)).applyTax(any(Item.class));
    }

    @Test
    public void shouldConvertToOutputEveryTaxedItem() {
        Item item = mock(Item.class);
        TaxedItem taxedItem = mock(TaxedItem.class);

        Item anotherItem = mock(Item.class);
        TaxedItem anotherTaxedItem = mock(TaxedItem.class);

        when(inputConverter.convert(anyString())).thenReturn(asList(item, anotherItem));

        when(taxCalculator.applyTax(item)).thenReturn(taxedItem);
        when(taxCalculator.applyTax(anotherItem)).thenReturn(anotherTaxedItem);

        sut.calculate(anyString());

        verify(outputConverter).convert(asList(taxedItem, anotherTaxedItem));
    }

    @Test
    public void shouldReturnConvertedOutput() {
        when(outputConverter.convert(anyListOf(TaxedItem.class))).thenReturn("test");

        String output = sut.calculate(anyString());

        assertThat(output).isEqualTo("test");
    }
}
