package com.expedia.test.receipt_service;

import com.expedia.test.receipt_service.dto.Item;
import com.expedia.test.receipt_service.dto.TaxedItem;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class TaxCalculatorTest {

    private final TaxCalculator sut = new TaxCalculator();

    @Test
    @Parameters({"1|0.75|0.13125", "2|0.75|0.2625", "3|0.75|0.39375"})
    public void shouldCalculateTaxOfMultipleItems(Integer quantity, Double price, Double expectedTax) {
        TaxedItem taxedItem = sut.applyTax(new Item(quantity, "test", price));

        assertThat(taxedItem.getTax()).isEqualTo(expectedTax);
    }

    @Test
    @Parameters({"1|music CD|1.38125", "1|video CD|1.38125", "1|variety CDs|1.38125", "2|CD|1.5125"})
    public void shouldApplyFixedTaxSurchargeToTaxForCDs(Integer quantity, String name, Double expectedTax) {
        TaxedItem taxedItem = sut.applyTax(new Item(quantity, name, 0.75));

        assertThat(taxedItem.getTax()).isEqualTo(expectedTax);
    }

    @Test
    @Parameters({"1|box of tooth ache pills|0.0", "2|thermometer|0.0", "1|thermometer|0.0"})
    public void shouldNotApplyTaxForMedicalProducts(Integer quantity, String name, Double expectedTax) {
        TaxedItem taxedItem = sut.applyTax(new Item(quantity, name, 0.75));

        assertThat(taxedItem.getTax()).isEqualTo(expectedTax);
    }

    @Test
    public void shouldCalculateTotalTaxOfAllPricedItems() {
        TaxedItem taxedItem = mock(TaxedItem.class);
        TaxedItem anotherTaxedItem = mock(TaxedItem.class);

        when(taxedItem.getTax()).thenReturn(1.0);
        when(anotherTaxedItem.getTax()).thenReturn(2.0);

        Double totalTax = sut.calculateTotalTax(asList(taxedItem, anotherTaxedItem));

        assertThat(totalTax).isEqualTo(3.0);
    }

    @Test
    public void shouldCalculateTotalPriceOfAllPricedItems() {
        TaxedItem taxedItem = mock(TaxedItem.class);
        TaxedItem anotherTaxedItem = mock(TaxedItem.class);

        when(taxedItem.getTotalPrice()).thenReturn(1.0);
        when(anotherTaxedItem.getTotalPrice()).thenReturn(2.0);

        Double total = sut.calculateTotal(asList(taxedItem, anotherTaxedItem));

        assertThat(total).isEqualTo(3.0);
    }
}
