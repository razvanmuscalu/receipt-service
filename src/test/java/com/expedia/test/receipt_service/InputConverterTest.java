package com.expedia.test.receipt_service;

import com.expedia.test.receipt_service.dto.Item;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(JUnitParamsRunner.class)
public class InputConverterTest {

    private final InputConverter sut = new InputConverter();

    @Test
    @Parameters({"one", "one book", "one book 5.99"})
    public void shouldThrowReceiptExceptionWhenNotEnoughArguments(String input) {
        assertThatThrownBy(() -> sut.convert(input))
            .isInstanceOf(ReceiptException.class)
            .hasMessage("Not enough arguments");
    }

    @Test
    public void shouldThrowReceiptExceptionWhenFirstArgumentNotANumber() {
        assertThatThrownBy(() -> sut.convert("book one at 5.99"))
            .isInstanceOf(ReceiptException.class)
            .hasMessage("First argument is not a number");
    }

    @Test
    public void shouldThrowReceiptExceptionWhenLastArgumentNotANumber() {
        assertThatThrownBy(() -> sut.convert("one book at price"))
            .isInstanceOf(ReceiptException.class)
            .hasMessage("Last argument is not a number");
    }

    @Test
    @Parameters({"one book at 29.49|1",
                 "two books at 29.49|2",
                 "three books at 29.49|3",
                 "four books at 29.49|4",
                 "five books at 29.49|5",
                 "six books at 29.49|6",
                 "seven books at 29.49|7",
                 "eight books at 29.49|8",
                 "nine books at 29.49|9"})
    public void shouldBeAbleToTransformEveryStringNumberFrom1To9IntoIntegerNumber(String input, Integer expectedQuantity) {
        List<Item> items = sut.convert(input);

        assertThat(items.get(0).getQuantity()).isEqualTo(expectedQuantity);
    }

    @Test
    @Parameters({"one book at 29.49|book",
                 "one chocolate snack at 0.75|chocolate snack"})
    public void shouldConcatenateWordsIntoTheItemName(String input, String expectedName) {
        List<Item> items = sut.convert(input);

        assertThat(items.get(0).getName()).isEqualTo(expectedName);
    }

    @Test
    public void shouldReadPriceIntoDouble() {
        List<Item> items = sut.convert("one book at 29.49");

        assertThat(items.get(0).getPrice()).isEqualTo(29.49);
    }

    @Test
    public void shouldReturnOneItemForEachGivenItemInInput() {
        List<Item> items = sut.convert("one book at 29.49\none car at 29.49");

        assertThat(items).hasSize(2);
        assertThat(items.get(0).getName()).isEqualTo("book");
        assertThat(items.get(1).getName()).isEqualTo("car");
    }
}
