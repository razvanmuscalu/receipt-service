package com.expedia.test.receipt_service;

import com.expedia.test.receipt_service.dto.Item;
import com.expedia.test.receipt_service.dto.TaxedItem;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(JUnitParamsRunner.class)
public class OutputConverterTest {

    @Mock
    private TaxCalculator taxCalculator;

    @InjectMocks
    private OutputConverter sut;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void shouldConvertPricedItemInCorrectFormat() {
        TaxedItem taxedItem = mock(TaxedItem.class);
        when(taxedItem.getItem()).thenReturn(mock(Item.class));
        when(taxedItem.getItem().getQuantity()).thenReturn(1);
        when(taxedItem.getItem().getName()).thenReturn("product1");
        when(taxedItem.getTotalPrice()).thenReturn(1.0);

        String output = sut.convert(singletonList(taxedItem));
        String[] outputLines = output.split("\n");

        assertThat(outputLines[0]).isEqualTo("1 product1: 1.00");
    }

    @Test
    @Parameters({"0.9|0.90", "0.100|0.10"})
    public void shouldUseTwoDigitsAfterDecimalForProductPrice(Double price, String expectedPrice) {
        TaxedItem taxedItem = mock(TaxedItem.class);
        when(taxedItem.getItem()).thenReturn(mock(Item.class));
        when(taxedItem.getItem().getQuantity()).thenReturn(1);
        when(taxedItem.getItem().getName()).thenReturn("product1");
        when(taxedItem.getTotalPrice()).thenReturn(price);

        String output = sut.convert(singletonList(taxedItem));
        String[] outputLines = output.split("\n");

        assertThat(outputLines[0]).isEqualTo("1 product1: " + expectedPrice);
    }

    @Test
    public void shouldConvertEveryPricedItemIntoANewLine() {
        TaxedItem taxedItem = mock(TaxedItem.class);
        when(taxedItem.getItem()).thenReturn(mock(Item.class));
        when(taxedItem.getItem().getQuantity()).thenReturn(1);
        when(taxedItem.getItem().getName()).thenReturn("product1");
        when(taxedItem.getTotalPrice()).thenReturn(1.0);

        TaxedItem anotherTaxedItem = mock(TaxedItem.class);
        when(anotherTaxedItem.getItem()).thenReturn(mock(Item.class));
        when(anotherTaxedItem.getItem().getQuantity()).thenReturn(2);
        when(anotherTaxedItem.getItem().getName()).thenReturn("product2");
        when(anotherTaxedItem.getTotalPrice()).thenReturn(2.0);

        String output = sut.convert(asList(taxedItem, anotherTaxedItem));
        String[] outputLines = output.split("\n");

        assertThat(outputLines[0]).isEqualTo("1 product1: 1.00");
        assertThat(outputLines[1]).isEqualTo("2 product2: 2.00");
    }

    @Test
    @Parameters({"0.9|0.90", "0.100|0.10"})
    public void shouldContainTotalTaxAmountUsingTwoDigitsAfterDecimalForPrice(Double totalTax, String expectedTotalTax) {
        when(taxCalculator.calculateTotalTax(any())).thenReturn(totalTax);

        String output = sut.convert(anyListOf(TaxedItem.class));
        String[] outputLines = output.split("\n");

        assertThat(outputLines[1]).isEqualTo("Taxes: " + expectedTotalTax);
    }

    @Test
    @Parameters({"0.9|0.90", "0.100|0.10"})
    public void shouldContainTotalPriceUsingTwoDigitsAfterDecimalForPrice(Double price, String expectedPrice) {
        when(taxCalculator.calculateTotal(any())).thenReturn(price);

        String output = sut.convert(anyListOf(TaxedItem.class));
        String[] outputLines = output.split("\n");

        assertThat(outputLines[2]).isEqualTo("Total: " + expectedPrice);
    }
}
