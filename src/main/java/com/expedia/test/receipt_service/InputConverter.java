package com.expedia.test.receipt_service;

import com.expedia.test.receipt_service.dto.Item;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static com.expedia.test.receipt_service.ReceiptService.NEW_LINE;
import static com.expedia.test.receipt_service.ReceiptService.SPACE;
import static java.lang.Double.parseDouble;
import static java.util.Collections.unmodifiableMap;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.of;

public class InputConverter {

    private static final Map<String, Integer> STRING_INTEGER_MAP;

    static {
        Map<String, Integer> aMap = new HashMap<>();
        aMap.put("one", 1);
        aMap.put("two", 2);
        aMap.put("three", 3);
        aMap.put("four", 4);
        aMap.put("five", 5);
        aMap.put("six", 6);
        aMap.put("seven", 7);
        aMap.put("eight", 8);
        aMap.put("nine", 9);
        STRING_INTEGER_MAP = unmodifiableMap(aMap);
    }

    public List<Item> convert(String input) {
        return of(input.split(NEW_LINE)).map(toItem()).collect(toList());
    }

    private Function<String, Item> toItem() {
        return line -> {
            String[] parts = line.split(SPACE);

            validateArgumentsLength(parts);

            Integer quantity = mapToQuantity(parts[0]);
            String name = mapToName(parts);
            Double price = mapToPrice(parts[parts.length - 1]);

            return new Item(quantity, name, price);
        };
    }

    private void validateArgumentsLength(String[] parts) {
        if (parts.length < 4) {
            throw new ReceiptException("Not enough arguments");
        }
    }

    private Integer mapToQuantity(String input) {
        if (STRING_INTEGER_MAP.containsKey(input)) {
            return STRING_INTEGER_MAP.get(input);
        }

        throw new ReceiptException("First argument is not a number");
    }

    private String mapToName(String[] parts) {
        return of(parts).skip(1).limit(parts.length - 3).collect(joining(SPACE));
    }

    private Double mapToPrice(String input) {
        try {
            return parseDouble(input);
        } catch (NumberFormatException ex) {
            throw new ReceiptException("Last argument is not a number");
        }
    }
}
