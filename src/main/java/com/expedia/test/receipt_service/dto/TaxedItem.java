package com.expedia.test.receipt_service.dto;

import static com.expedia.test.receipt_service.MathFunctions.roundToUpper05;

public class TaxedItem {

    private final Item item;
    private final Double tax;

    public TaxedItem(Item item, Double tax) {
        this.item = item;
        this.tax = tax;
    }

    public Item getItem() {
        return item;
    }

    public Double getTax() {
        return tax;
    }

    public Double getTotalPrice() {
        return roundToUpper05(item.getQuantity() * item.getPrice() + tax).apply();
    }
}
