package com.expedia.test.receipt_service.dto;

public class Item {

    private final Integer quantity;
    private final String name;
    private final Double price;

    public Item(Integer quantity, String name, Double price) {
        this.quantity = quantity;
        this.name = name;
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }
}
