package com.expedia.test.receipt_service;

import com.expedia.test.receipt_service.dto.Item;
import com.expedia.test.receipt_service.dto.TaxedItem;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class ReceiptService {

    public static final String SPACE = " ";
    public static final String NEW_LINE = "\n";

    private final InputConverter inputConverter;
    private final TaxCalculator taxCalculator;
    private final OutputConverter outputConverter;

    public ReceiptService(InputConverter inputConverter,
                          TaxCalculator taxCalculator,
                          OutputConverter outputConverter) {
        this.inputConverter = inputConverter;
        this.taxCalculator = taxCalculator;
        this.outputConverter = outputConverter;
    }

    public String calculate(String input) {
        List<Item> items = inputConverter.convert(input);

        List<TaxedItem> taxedItems = items.stream().map(taxCalculator::applyTax).collect(toList());

        return outputConverter.convert(taxedItems);
    }
}