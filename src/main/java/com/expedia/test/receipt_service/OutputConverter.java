package com.expedia.test.receipt_service;

import com.expedia.test.receipt_service.dto.TaxedItem;

import java.util.List;
import java.util.function.Function;

import static com.expedia.test.receipt_service.ReceiptService.NEW_LINE;
import static com.expedia.test.receipt_service.ReceiptService.SPACE;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class OutputConverter {

    private final TaxCalculator taxCalculator;

    public OutputConverter(TaxCalculator taxCalculator) {
        this.taxCalculator = taxCalculator;
    }

    public String convert(List<TaxedItem> taxedItems) {
        String totalTax = "Taxes:" + SPACE + format("%.2f", taxCalculator.calculateTotalTax(taxedItems));
        String total = "Total:" + SPACE + format("%.2f", taxCalculator.calculateTotal(taxedItems));

        return taxedItems.stream().map(toLine()).collect(joining(NEW_LINE)) + NEW_LINE + totalTax + NEW_LINE + total;
    }

    private Function<TaxedItem, String> toLine() {
        return pricedItem -> pricedItem.getItem().getQuantity()
                             + SPACE
                             + pricedItem.getItem().getName()
                             + ":"
                             + SPACE
                             + format("%.2f", pricedItem.getTotalPrice());
    }
}
