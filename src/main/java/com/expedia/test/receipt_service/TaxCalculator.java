package com.expedia.test.receipt_service;

import com.expedia.test.receipt_service.dto.Item;
import com.expedia.test.receipt_service.dto.TaxedItem;

import java.util.List;

import static com.expedia.test.receipt_service.MathFunctions.calculateTax;
import static com.expedia.test.receipt_service.ReceiptService.SPACE;
import static java.util.Arrays.asList;
import static java.util.stream.Stream.of;

public class TaxCalculator {

    private static final List<String> MEDICAL_KEYWORDS = asList("pills", "thermometer");
    private static final List<String> CD_KEYWORDS = asList("CD", "CDs");

    private static final Double MEDICAL_TAX = 0.0;
    private static final Double NORMAL_TAX_PERCENTAGE = 17.5;
    private static final Double CD_FIXED_TAX_SURCHARGE = 1.25;

    public TaxedItem applyTax(Item item) {
        boolean isMedicalProduct = of(item.getName().split(SPACE)).anyMatch(MEDICAL_KEYWORDS::contains);
        if (isMedicalProduct) {
            return new TaxedItem(item, MEDICAL_TAX);
        }

        Double tax = calculateTax(item.getQuantity() * item.getPrice(), NORMAL_TAX_PERCENTAGE).apply();

        boolean isCD = of(item.getName().split(SPACE)).anyMatch(CD_KEYWORDS::contains);
        if (isCD) {
            tax += CD_FIXED_TAX_SURCHARGE;
        }

        return new TaxedItem(item, tax);
    }

    public Double calculateTotalTax(List<TaxedItem> taxedItems) {
        return taxedItems.stream().mapToDouble(TaxedItem::getTax).sum();
    }

    public Double calculateTotal(List<TaxedItem> taxedItems) {
        return taxedItems.stream().mapToDouble(TaxedItem::getTotalPrice).sum();
    }
}
