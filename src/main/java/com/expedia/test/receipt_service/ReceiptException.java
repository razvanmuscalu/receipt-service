package com.expedia.test.receipt_service;

public class ReceiptException extends RuntimeException {

    public ReceiptException(String message) {
        super(message);
    }
}
