package com.expedia.test.receipt_service;

import static java.lang.Math.round;

@FunctionalInterface
public interface MathFunctions {

    double apply();

    static MathFunctions calculateTax(double price, double taxPercentage) {
        return () -> price * taxPercentage / 100;
    }

    static MathFunctions roundToUpper05(double number) {
        return () -> round(number * 20.0) / 20.0;
    }

}
