# receipt-service

A simple application that prints out the details of a receipt containing items purchased by a customer.

## How To Run

`mvn install` - to run all tests

There is also an IntelliJ runner committed `RunTests`.

## General Design

Three main business logic classes:
 - `InputConverter` (responsible for parsing input)
 - `TaxCalculator` (responsible for applying tax to items)
 - `OutputConverter` (responsible for converting output in the correct format)
 
`ReceiptService` is a main class delegating to the above classes. Because I opted to validate input upfront and fail early, the rest of the code following input validation did not have to be hardened against fail scenarios (e.g. _NullPointerException_, etc.).
 
## Limitations

- Can only parse product quantities ranging from 1 to 9
  - A more complex solution is required for more complex quantities (e.g. _twenty three_)
- Some hardcoding around the input structure (e.g. calculating the item name strictly around the _at_ word)
- Keywords for recognizing CD or medical products are hardcoded into the `TaxCalculator` class and the solution around this problem was kept quite rudimentary and simple.